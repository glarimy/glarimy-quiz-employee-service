package com.glarimy.cloud.employee.data;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.glarimy.cloud.employee.domain.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String> {

}
