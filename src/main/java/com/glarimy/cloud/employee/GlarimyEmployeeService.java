package com.glarimy.cloud.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GlarimyEmployeeService {

	public static void main(String[] args) {
		SpringApplication.run(GlarimyEmployeeService.class, args);
	}

}
